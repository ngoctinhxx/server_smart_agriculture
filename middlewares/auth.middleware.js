const jwt = require('jsonwebtoken');

const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        if (!token) {
            const error = new Error('Invalid access token.');
            error.status = 401; // Unauthorized
            return next(error);
        };
        console.log('Authorization', token);
        let decoded = await jwt.verify(token, process.env.ACCESS_TOKEN_PRIVATE_KEY);
        req["user"] = decoded.data;
        next();
    } catch (err) {
        const error = new Error('Invalid access token.');
        error.status = 401; // Unauthorized
        return next(error);
    }
}

module.exports = auth;