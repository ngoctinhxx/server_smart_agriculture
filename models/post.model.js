const mongoose = require('mongoose');

const PostSchema = mongoose.Schema({
    title: {
        type: String,
        required : true, 
        index: { unique: true }
    },
    description: {
        type: String,
        default: ''
    },
    image: {
        type: String,
        default: ''
    }
},{timestamp: true});

module.exports = mongoose.model('POST', PostSchema);