const {PostModel} = require('../models/index');

const getAllPosts = () => {
    return PostModel.find();
}

const getPostDetail = (id) =>{
    return PostModel.findById(id);
}

const postCreatePost = (data) => {
    const newPost = new PostModel(data);
    return newPost.save();
}

const putUpdatePost  = (id, data) =>{
    return PostModel.updateOne({_id: id}, {$set: data});
}

const deleteAllPosts = () =>{
    return PostModel.deleteMany();
}

const deletePost = (id) =>{
    return PostModel.findOneAndDelete({_id: id});
}

module.exports = {getAllPosts, getPostDetail,postCreatePost, putUpdatePost, deleteAllPosts, deletePost}