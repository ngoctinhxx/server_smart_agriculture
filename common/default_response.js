const defaultResponse = {
    message: 'success',
    error: null,
    status: true,
    result: null,
}

module.exports = defaultResponse;