const express = require("express");
require("dotenv").config();
const cors = require("cors");
var bodyParser = require("body-parser");
const routes = require("./routes/index");
const connectDatabase = require("./database/connect_database");

//connect database
connectDatabase();

const app = express();
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

routes(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  console.log(err.message);
  // render the error page
  res.status(err.status || 500);
  res.json({ message: err.message, status: err.status });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log("listening on port", PORT);
});
