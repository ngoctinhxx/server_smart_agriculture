const postRoute = require('./post.route');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');

const routes = app =>{
    app.use('/api/posts', postRoute);
    app.use('/api/auth', authRoute);
    app.use('/api/users', userRoute);
    app.get('/',(req, res)=>{
        res.send('Hello, world!!!');
    });
}

module.exports = routes;