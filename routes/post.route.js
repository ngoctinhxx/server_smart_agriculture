const {Router} = require('express');
const {PostController} = require('../controllers/index');
const router = Router();

//[GET] Get all posts
router.get('/',PostController.getAllPosts);

//[GET] get post detail
router.get('/:id', PostController.getPostDetail);

//[POST] post create a new post
router.post('/create',PostController.postCreatePost);
//[PUT] post update a post
router.put('/update/:id',PostController.putUpdatePost);

//[DELETE] delete a post
router.delete('/delete/:id',PostController.deletePost);

//[DELETE] delete all posts
router.delete('/delete',PostController.deleteAllPosts);

module.exports = router;