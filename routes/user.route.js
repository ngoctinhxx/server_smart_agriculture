var express = require('express');
var router = express.Router();
const {UserController} = require('../controllers/index');
const {auth} = require('../middlewares/index');

/* GET users listing. */
router.get('/',auth, UserController.getUsers);

module.exports = router;
