var express = require('express');
var router = express.Router();
const {AuthController} = require('../controllers/index');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource Auth');
});
/* [POST] login. */
router.post('/login', AuthController.login);

/* [POST] register*/ 
router.post('/register', AuthController.register);

module.exports = router;
