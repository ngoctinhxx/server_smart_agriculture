const {PostService}  = require('../services/index');

const defaultResult = {
    message: 'success',
    error: null,
    status: true,
    result: null
}

const getAllPosts = async (req, res)=>{
    try {
        const data = await PostService.getAllPosts();
        const result = {...defaultResult, result: data};
        return res.status(200).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, status: false, message: 'failed'};
        return res.status(500).json(result);
    }
}

const getPostDetail = async (req, res)=>{
    try {
        const {id} = req.params;
        const data = await PostService.getPostDetail(id);
        if(!data) throw new Error('Not found post detail');
        const result = {...defaultResult, result: data};
        return res.status(200).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, status: false, message: 'failed'};
        return res.status(500).json(result);
    }
}

const postCreatePost = async (req, res)=>{
    try {
        const body = req.body
        const data = await PostService.postCreatePost(body);
        const result = {...defaultResult, result: data};
        return res.status(201).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, status: false, message: 'failed'};
        return res.status(500).json(result);
    }
}

const putUpdatePost = async (req, res)=>{
    try {
        const {id} = req.params;
        const body = req.body
        const data = await PostService.putUpdatePost(id, body);
        const result = {...defaultResult, result: data};
        return res.status(200).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, status: false, message: 'failed'};
        return res.status(500).json(result);
    }
}

const deleteAllPosts = async (req, res)=>{
    try {
        const data = await PostService.deleteAllPosts();
        const result = {...defaultResult, result: data};
        return res.status(200).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, status: false, message: 'failed'};
        return res.status(500).json(result);
    }
}

const deletePost = async (req, res)=>{
    try {
        const {id} = req.params;
        const data = await PostService.deletePost(id);
        if(!data) throw new Error('Not found post delete');
        const result = {...defaultResult, result: data};
        return res.status(200).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, status: false, message: 'failed'};
        return res.status(500).json(result);
    }
}

module.exports = {getAllPosts, getPostDetail, postCreatePost, putUpdatePost, deleteAllPosts, deletePost}