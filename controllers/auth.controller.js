const {UserService} = require('../services/index');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const validate = require('../utils/validate');
const jwt = require('jsonwebtoken');

const defaultResult = {
    message: 'success',
    error: null,
    result: null
}
const register = async (req, res, next) =>{
    try {
        const body = req.body;
        let newData = {...body};
        if(!newData.password || newData.password.length < 6){
            throw new Error('Password must be at least 6 characters');
        }

        if(!newData.email || !validate.isEmail(newData.email)){
            throw new Error('Email is wrong');
        }
       
        const emailExisted = await UserService.findByEmail(newData.email);
        if(emailExisted){
            throw new Error('Email already exists');
        }
        const hashPass = bcrypt.hashSync(newData.password, saltRounds);
        newData = {...newData, password: hashPass}

        const newUser = await UserService.create(newData);
        const token = generateTokens({id: newUser._id, role: newUser.role});
        const result = {...defaultResult, result: {...token, user:newUser}};
        res.status(201).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, message: 'failed'};
        return res.status(500).json(result);
    }
}

const login = async (req, res, next) =>{
    try {
        const body = req.body;
        if(!body.email || !validate.isEmail(body.email)){
            throw new Error('Email is wrong');
        }
        if(!body.password || body.password.length < 6){
            throw new Error('Password is wrong');
        }
        const user = await UserService.findByEmail(body.email);
        if(!user){
            throw new Error('Email is not exists');
        }
        let checkPass = await bcrypt.compare(body.password, user.password);
        if(checkPass){
            const token = generateTokens({id: user._id, role: user.role});
            const result = {...defaultResult, result: {...token, user}};
            res.status(200).json(result);

        }else{
            throw new Error('Password is wrong');
        }
    } catch (error) {
        const result = {...defaultResult, error: error.message, message: 'failed'};
        res.status(500).json(result);
    }
}


const getAccessToken = (payload) =>{
    return jwt.sign(
        payload,
        process.env.ACCESS_TOKEN_PRIVATE_KEY,
        { expiresIn: 60 * 60 * 24 * 7} 
    );
}

const getRefreshToken = (payload) =>{
    return jwt.sign(
        payload,
        process.env.REFRESH_TOKEN_PRIVATE_KEY,
        { expiresIn: 60 * 60 * 24 * 30 } 
    );
}

const generateTokens = (payload) =>{
    return {accessToken: getAccessToken(payload), refreshToken: getRefreshToken(payload)};
}


// const logout = async (req, res, next) =>{
//     if(req.cookies.user){
//         res.clearCookie('user');
//     }
//     res.redirect('/auth/login');
// }


module.exports = {login, register};