const {UserService} = require('../services/index');

const defaultResult = {
    message: 'success',
    error: null,
    result: null
}

const getUsers = async (req, res, next) =>{
    try {
        const users = await UserService.findAll();
        const result = {...defaultResult, result: users};
        res.status(200).json(result);
    } catch (error) {
        const result = {...defaultResult, error: error.message, message: 'failed'};
        return res.status(500).json(result);
    }
}

module.exports = {getUsers};