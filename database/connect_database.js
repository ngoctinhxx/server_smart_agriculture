require('dotenv').config();
const mongoose = require('mongoose');

const connect = () =>{
    mongoose.connect(process.env.MONGO_URL).then(()=>{
        console.log('connected to mongodb');
    });
}

module.exports = connect;